//
//  AnnotationViewController.m
//  LocationTest
//
//  Created by Winparf on 15/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "AnnotationViewController.h"
#import "EmptyView.h"

@interface AnnotationViewController () <UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property (nonatomic,strong)EmptyView *emptyView;
@property (nonatomic,strong)NSMutableArray *pinImagesArray;
@property (nonatomic,strong)UIImage *pinImage;
@property(nonatomic)NSNumber *numberPinImage;
@property (nonatomic)BOOL isEdited;

@end

@implementation AnnotationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imagePicker = [[UIImagePickerController alloc]init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.pinImagesArray = [[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"pinIcon"],[UIImage imageNamed:@"LocationIcon"],[UIImage imageNamed:@"AddLocation"],nil];
    
    self.titleTextField.delegate = self;
    self.subtitleTextView.delegate = self;

    CAGradientLayer *gradientLayerPhoto = [CAGradientLayer layer];
    gradientLayerPhoto.frame = self.getPhotoButton.layer.bounds;
    
    gradientLayerPhoto.colors = [NSArray arrayWithObjects:
                            (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
                            (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
                            nil];
    
    gradientLayerPhoto.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    self.getPhotoButton.layer.cornerRadius = 15.0;
    gradientLayerPhoto.cornerRadius = self.getPhotoButton.layer.cornerRadius;
    [self.getPhotoButton setContentMode:UIViewContentModeCenter];
    self.getPhotoButton.clipsToBounds = YES;
    [self.getPhotoButton.layer addSublayer:gradientLayerPhoto];

    
    CAGradientLayer *gradientLayerCamera = [CAGradientLayer layer];
    gradientLayerCamera.frame = self.getPhotoButton.layer.bounds;
    
    gradientLayerCamera.colors = [NSArray arrayWithObjects:
                                 (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
                                 (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
                                 nil];
    
    gradientLayerCamera.locations = [NSArray arrayWithObjects:
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    nil];
    
    self.getCameraButton.layer.cornerRadius = 15.0;
    gradientLayerCamera.cornerRadius = self.getCameraButton.layer.cornerRadius;
    [self.getCameraButton setContentMode:UIViewContentModeCenter];
    self.getCameraButton.clipsToBounds = YES;
    [self.getCameraButton.layer addSublayer:gradientLayerCamera];
    
    
    CAGradientLayer *gradientLayerPin = [CAGradientLayer layer];
    gradientLayerPin.frame = self.getPhotoButton.layer.bounds;
    
    gradientLayerPin.colors = [NSArray arrayWithObjects:
                                 (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
                                 (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
                                 nil];
    
    gradientLayerPin.locations = [NSArray arrayWithObjects:
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    nil];

    self.pinImageButton.layer.cornerRadius = 15.0;
    gradientLayerPin.cornerRadius = self.pinImageButton.layer.cornerRadius;
    [self.pinImageButton setContentMode:UIViewContentModeCenter];
    self.pinImageButton.clipsToBounds = YES;
    [self.pinImageButton.layer addSublayer:gradientLayerPin];
    
    self.imageView.backgroundColor = [UIColor lightGrayColor];
    self.imageView.layer.cornerRadius = 30;
    
    self.view.backgroundColor = [UIColor colorWithRed:100/255.0 green:100/255.0 blue:100/255.0 alpha:1.0];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:100/255.0 green:100/255.0 blue:100/255.0 alpha:1.0];
    
    [self.filterSegmentedControl addTarget:self action:@selector(segmentedControlValueChange) forControlEvents:UIControlEventValueChanged];
    
    if (self.annotation) {

        [self.pinImageButton setTitle:@"" forState:UIControlStateNormal];
        [self.pinImageButton setImage:[self.pinImagesArray objectAtIndex:[self.annotation.pinImageIndex intValue]] forState:UIControlStateNormal];
        self.imageView.image = self.annotation.photo;
        self.titleTextField.text = self.annotation.title;
        self.subtitleTextView.text = self.annotation.subtitle;
        self.dateLabel.text = self.annotation.dateString;
         self.filterSegmentedControl.selectedSegmentIndex = self.annotation.filterIndex;
        self.imageView.alpha = 1;
        
    }else{
        
        [self.pinImageButton setTitle:@"" forState:UIControlStateNormal];
        [self.pinImageButton setImage:[UIImage imageNamed:@"pinIcon"] forState:UIControlStateNormal];
        self.numberPinImage = 0;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"d.M.yyyy";
        self.dateLabel.text = [formatter stringFromDate:[NSDate date]];
        self.filterSegmentedControl.selectedSegmentIndex = 3;
        self.imageView.alpha = 0.6;
    }
    self.dateLabel.textColor = [UIColor lightTextColor];
    self.imageView.layer.cornerRadius = 30;
    self.imageView.clipsToBounds = YES;
    [self.view addSubview: self.imageView];
    
    if (!self.canEdit) {
        self.getPhotoButton.enabled = NO;
        self.getCameraButton.enabled = NO;
        self.pinImageButton.enabled = NO;
        self.titleTextField.enabled = NO;
        self.subtitleTextView.editable = NO;
        self.filterSegmentedControl.enabled = NO;
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        self.getPhotoButton.enabled = YES;
        self.getCameraButton.enabled = YES;
        self.pinImageButton.enabled = YES;
        self.titleTextField.enabled = YES;
        self.subtitleTextView.editable = YES;
        self.filterSegmentedControl.enabled = YES;
        self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleDone;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    if (!self.imageView.image && !self.emptyView) {
        self.emptyView = [[EmptyView alloc]initWithFrame:CGRectMake(0, 0 , self.imageView.frame.size.width * 1/2, self.imageView.frame.size.height) mainText:@"Load an Image" andImage:[UIImage imageNamed:@"ImageFile"]];
        self.emptyView.alpha = 0.6;
        self.imageView.layer.cornerRadius = 30;
        [self.imageView addSubview:self.emptyView];
    }else if (self.imageView.image){
        self.imageView.layer.cornerRadius = 30;
        [self.emptyView removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)segmentedControlValueChange{
    self.isEdited = YES;
}

- (IBAction)doneButtonPressed:(id)sender {
    if (!self.canEdit) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
    
        if (![self.titleTextField.text length]) {
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Please write a Title"
                                                                  message:nil
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];    
            [myAlertView show];
        }else{
            
            NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"objectId"];
            
            if (!objectId.length > 0) {
                objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"objectId"];
            }
            
            if (!self.annotation) {
                
                [self.delegate didSaveNewAnnotationWithImage:self.imageView.image TitleAnnotation:self.titleTextField.text Subtitle:self.subtitleTextView.text Filter:self.filterSegmentedControl.selectedSegmentIndex Owner: objectId PinImageIndex:self.numberPinImage];
            }else{
                if (self.isEdited) {
                    self.annotation.photo = self.imageView.image;
                    self.annotation.title = self.titleTextField.text;
                    self.annotation.subtitle = self.subtitleTextView.text;
                    self.annotation.filterIndex = self.filterSegmentedControl.selectedSegmentIndex;
                    self.annotation.owner = objectId;
                    self.annotation.pinImageIndex = self.numberPinImage;
                    
                    NSString *stringFilter;
                    switch (self.filterSegmentedControl.selectedSegmentIndex) {
                        case 0:
                            stringFilter = @"You";
                            break;
                        case 1:
                            stringFilter = @"Couple";
                            break;
                        case 2:
                            stringFilter = @"Friends";
                            break;
                        case 3:
                            stringFilter = @"Public";
                            break;
                            
                        default:
                            break;
                    }
                    
                    self.annotation.filter = stringFilter;
                    
                    [self.delegate didUpdateAnnotation:self.annotation];
                    self.annotation = nil;
                }
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)getPhotoButtonPressed:(id)sender {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)getCameraButton:(id)sender {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }
}

- (IBAction)pinImageButtonPressed:(id)sender {
    UIButton *pinImageButton = (UIButton *)sender;
    
    self.isEdited = YES;
    int value;
    if ([self.numberPinImage intValue] < self.pinImagesArray.count - 1) {
        value = [self.numberPinImage intValue];
        value ++;
        self.numberPinImage = [NSNumber numberWithInt:value];
        [self.pinImageButton setTitle:@"" forState:UIControlStateNormal];
        [self.pinImageButton setImage:[self.pinImagesArray objectAtIndex:value] forState:UIControlStateNormal];
    }else{
        
        value = 0;
        self.numberPinImage = [NSNumber numberWithInt:value];
        pinImageButton.imageView.image = nil;
        [self.pinImageButton setImage:nil forState:UIControlStateNormal];
        [self.pinImageButton setTitle:@"" forState:UIControlStateNormal];
        [self.pinImageButton setImage:[self.pinImagesArray objectAtIndex:value] forState:UIControlStateNormal];
        
    }
    self.pinImage = [self.pinImagesArray objectAtIndex:value];
}

-(BOOL)shouldAutorotate{
    
    return YES;
}

#pragma mark UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.imageView.alpha = 1;
    self.imageView.image = chosenImage;
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.isEdited = YES;
    
}

#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.isEdited = YES;
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        self.view.frame = CGRectMake(0, self.view.frame.size.height/5 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.view.frame = CGRectMake(0, self.view.frame.size.height/3 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.titleTextField) {
        [self.subtitleTextView becomeFirstResponder];
    }else{
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark UITextView Delegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.isEdited = YES;
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        self.view.frame = CGRectMake(0, self.view.frame.size.height/5 - textView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.view.frame = CGRectMake(0, self.view.frame.size.height/3- self.titleTextField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
@end
