//
//  RequestManager.h
//  LocationTest
//
//  Created by Winparf on 15/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "MyAnnotation.h"
#import <Parse/Parse.h>
@protocol RequestManagerDelegate

//User methods delegate

-(void)didUpdateUser:(BOOL)succeeded;

-(void)didReceivedUser:(PFObject *)user;

//Annotations methods delegate
-(void)didReceivedYourLocations:(NSMutableArray *)locations;

-(void)didReceivedAllLocations:(NSMutableArray *)locations;

-(void)didGetNumberOfLocations:(NSUInteger)number;

-(void)didSavedLocation:(MyAnnotation *)annotation;

-(void)didUpdatedLocation:(MyAnnotation *)annotation;

-(void)didDeletedAnnotation:(BOOL)succeeded;

//Friends methods delegate

-(void)didGetFriendsForUser:(NSMutableArray *)userIds;

-(void)didReceivedUserForFriend:(PFObject *)user;

-(void)didReceivedFriend:(PFObject *)user;

//Couple methods delegate

-(void)didGetCouple:(NSString *) couple;

-(void)didReceivedUserForCouple:(PFObject *)user;

-(void)didSetDefaultImage:(BOOL)succeeded;

//Reachability method delegate

-(void)hasNoConnection;


@end

@interface RequestManager : NSObject

@property (nonatomic,strong) id <RequestManagerDelegate> delegate;

//User methods

-(void)updateUserWithUsername:(NSString *)username Couple:(NSDictionary *)couple Email:(NSString *)email;

-(void)setDefaultImageProfile:(NSString *) objectId;

-(void)getUserWithId:(NSString *)objectId;

//Annotations methods

-(void)getYourLocations;

-(void)getAllLocationsWithFilter: (NSString *)filterString;

-(void)numberOfLocationsForUser;

-(void)saveLocation:(MyAnnotation *)newAnnotation;

-(void)updateLocation:(MyAnnotation *)annotation;

-(void)deleteAnnotationWithObjectId:(MyAnnotation *)annotation;

//Friends methods

-(void)checkUserForAddFriend:(NSString *)objectId;

-(void)getFriendsForUser;

-(void)getFriendById:(NSString *)objectId;

-(void)saveFriendRelationWithId:(NSString *)objectId;

-(void)deleteFirstFriendRelationWithId:(NSString *)objectId;

-(void)deleteSecondFriendRelationWithId:(NSString *)objectId;

//Couple methods
-(void) checkUserForAddCouple:(NSString *)userString WithFlag:(BOOL)isProfile;

-(void)getCoupleForUser;

-(void)saveCoupleRelationWithId:(NSString *)objectId;

-(void)deleteFirstCoupleRelationWithId:(NSString *)objectId;

-(void)deleteSecondCoupleRelationWithId:(NSString *)objectId;

@end
