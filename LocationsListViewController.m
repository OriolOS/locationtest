//
//  LocationsListViewController.m
//  LocationTest
//
//  Created by Oriol Orra Serra on 14/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

@import GoogleMobileAds;
#import "LocationsListViewController.h"
#import "SWTableViewCell.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AnnotationViewController.h"
#import "MyAnnotation.h"
#import "RequestManager.h"

@interface LocationsListViewController () <UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate, RequestManagerDelegate,AnnotationViewDelegate>
@property (nonatomic,strong) RequestManager *manager;
@property (nonatomic,strong) NSIndexPath *indexToDelete;

@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;

@end

@implementation LocationsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.manager = [[RequestManager alloc]init];
    self.manager.delegate = self;
    
    if (!_locationsArray) {
        _locationsArray = [[NSMutableArray alloc]init];
    }
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    // Replace this ad unit ID with your own ad unit ID.
    [self.adBannerView setAdUnitID:@"ca-app-pub-8715619194809926/6780050496"];
    self.adBannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [self.adBannerView loadRequest:request];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doneButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.locationsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyAnnotation *annotation = [[MyAnnotation alloc]init];
    annotation = [self.locationsArray objectAtIndex:indexPath.row];
    
    SWTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"locationCell"];
    cell.delegate = self;
    
    
    
    cell.textLabel.text = annotation.title;
    cell.detailTextLabel.text = annotation.subtitle;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    
    cell.imageView.image = annotation.photo;
    cell.imageView.layer.cornerRadius = 5;
    cell.imageView.clipsToBounds = YES;
    
    cell.rightUtilityButtons = [self rightButtons];
    cell.leftUtilityButtons = [self leftButtons];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0;
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyAnnotation *selectedAnnotation = [self.locationsArray objectAtIndex:indexPath.row];
    
    UINavigationController *annotationNC = [self.storyboard instantiateViewControllerWithIdentifier:@"annotationNC"];
    AnnotationViewController *annotationVC = (AnnotationViewController *)annotationNC.topViewController;
    annotationVC.annotation = selectedAnnotation;
    annotationVC.canEdit = YES;
    annotationVC.delegate = self;
    
    [self presentViewController:annotationNC animated:YES completion:nil];
}

#pragma mark SWTableviewCell

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.indexToDelete = indexPath;
    MyAnnotation *annotation = [self.locationsArray objectAtIndex:indexPath.row];
    
    [self.manager deleteAnnotationWithObjectId:annotation];
}

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MyAnnotation *annotation = [self.locationsArray objectAtIndex:indexPath.row];
    
    if([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]){
        
        #warning         TO POST ANNOTATION IN TIMELINE WHEN THE APP WILL BE FINISHED AND PUBLISHIED IN APPSTORE
        
        NSMutableArray *array = [NSMutableArray arrayWithObject:@"publish_actions"];
        [PFFacebookUtils logInInBackgroundWithPublishPermissions:array block:^(PFUser *user, NSError *error){
            
        }];
        
        NSString *title = [NSString stringWithFormat:@"LocationTest annotation -> %@",annotation.title];
        NSNumber *lat = [NSNumber numberWithFloat: annotation.coordinate.latitude];
        NSNumber *lon = [NSNumber numberWithFloat: annotation.coordinate.longitude];
        
//        NSDictionary *properties = @{
//                                     @"og:type": @"locationtest_test:annotation",
//                                     @"og:title": @"Sample Annotation",
//                                     @"og:description": title,
//                                     @"locationtest_test:point_lock:latitude":lat,
//                                     @"locationtest_test:point_lock:longitude":lon
//                                     };
//        FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
//        
//        FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
//        action.actionType = @"locationtest_test:hang";
//        [action setObject:object forKey:@"annotation"];
//        
//        FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
//        content.action = action;
//        content.previewPropertyName = @"annotation";
//
//        [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];

        NSDictionary *lock = @{
                                     @"og:type": @"locationtest_test:lock",
                                     @"og:title": @"Sample Annotation",
                                     @"og:description": title,
                                     @"place:location:latitude":lat,
                                     @"place:location:longitude":lon,
                                     @"locationtest_test:lock_place:latitude":lat,
                                     @"locationtest_test:lock_place:longitude":lon
                                     
                                     };
        FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:lock];

        
        FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
        action.actionType = @"locationtest_test:hang";
        [action setObject:object forKey:@"lock"];
        
        FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
        content.action = action;
        content.previewPropertyName = @"locationtest_test.lock";
        
        [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
        
        
//        NSDictionary *properties = @{
//                                     @"og:type": @"fitness.course",
//                                     @"og:title": title,
//                                     @"og:description": annotation.subtitle,
//                                     @"og:image":@"http://files.parsetfss.com/52ba98b4-52f5-4143-ab6a-fbf27642709b/tfss-4b8fed12-6ebf-440f-9acb-356be3a70b4d-file",
//                                     @"fitness:duration:value": @0,
//                                     @"fitness:duration:units": @"s",
//                                     @"fitness:distance:value": @0,
//                                     @"fitness:distance:units": @"km",
//                                     @"fitness:speed:value": @0,
//                                     @"fitness:speed:units": @"m/s",
//                                     @"fitness:metrics:location:latitude":lat,
//                                     @"fitness:metrics:location:longitude":lon,
//                                     };
//        FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
//        FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
//        action.actionType = @"fitness.walks";
//        [action setObject:object forKey:@"fitness:course"];
//        FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
//        content.action = action;
//        content.previewPropertyName = @"fitness:course";
//        
//        [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
        
        
    }else{
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"This user is not logged in Facebook"
                                              message:@"Please log with Facebook"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"Delete.png"]];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:0.7] icon:[UIImage imageNamed:@"Share.png"]];
    return leftUtilityButtons;
}

#pragma mark AnnotationView Delegate
-(void)didUpdateAnnotation:(MyAnnotation *)annotation{
    
    [self.manager updateLocation:annotation];
    
}

#pragma mark RequestManager Delegate

-(void)didDeletedAnnotation:(BOOL)succeeded{
   
    if (succeeded) {
        [self.locationsArray removeObjectAtIndex:self.indexToDelete.row];
        [self.tableView reloadData];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Location could not be deleted"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

-(void)didUpdatedLocation:(MyAnnotation *)annotation{
    
    [self.locationsArray removeObject:annotation];
    [self.locationsArray addObject:annotation];
    [self.tableView reloadData];
}

-(void)hasNoConnection{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Device has no internet connection"
                                          message:@"Please connect device to internet"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

@end
