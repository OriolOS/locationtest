//
//  ProfileUserViewController.m
//  LocationTest
//
//  Created by Winparf on 16/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "ProfileUserViewController.h"
#import "RequestManager.h"
#import "EmptyView.h"

@interface ProfileUserViewController () <RequestManagerDelegate, UITextFieldDelegate>
@property (nonatomic,strong) RequestManager *manager;
@property (nonatomic,strong) UIImagePickerController *imagePicker;
@property (nonatomic,strong)EmptyView *emptyView;
@property (nonatomic,strong)NSDictionary *coupleDict;

@end

@implementation ProfileUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.manager = [[RequestManager alloc]init];
    self.manager.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.coupleDict = [[NSDictionary alloc]init];
    
    self.imagePicker = [[UIImagePickerController alloc]init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    
    self.usernameTextfield.delegate = self;
    self.coupleTextfield.delegate = self;
    self.emailTextfield.delegate = self;
    
    [self.coupleTextfield addTarget:self action:@selector(getUserCouple:) forControlEvents:UIControlEventEditingDidEnd];
    
    
    NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    if (userName.length > 0) {
        self.usernameTextfield.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        self.emailTextfield.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"email"];
        self.coupleDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"couple"];
        
    }else{

        self.usernameTextfield.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"name"];
        self.emailTextfield.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"email"];
        self.coupleDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"couple"];
        
    }
    self.coupleTextfield.text = [self.coupleDict objectForKey:@"username"];
    self.usernameTextfield.textColor = [UIColor lightGrayColor];
    self.coupleTextfield.textColor = [UIColor lightGrayColor];
    self.emailTextfield.textColor = [UIColor lightGrayColor];

    CAGradientLayer *gradientLayerPhoto = [CAGradientLayer layer];
    gradientLayerPhoto.frame = self.getPhotoButton.layer.bounds;
    
    gradientLayerPhoto.colors = [NSArray arrayWithObjects:
                                 (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
                                 (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
                                 nil];
    
    gradientLayerPhoto.locations = [NSArray arrayWithObjects:
                                    [NSNumber numberWithFloat:0.0f],
                                    [NSNumber numberWithFloat:1.0f],
                                    nil];
    self.getPhotoButton.layer.cornerRadius = 15.0;
    gradientLayerPhoto.cornerRadius = self.getPhotoButton.layer.cornerRadius;
    [self.getPhotoButton setContentMode:UIViewContentModeCenter];
    self.getPhotoButton.clipsToBounds = YES;
    [self.getPhotoButton.layer addSublayer:gradientLayerPhoto];
    
    
    CAGradientLayer *gradientLayerCamera = [CAGradientLayer layer];
    gradientLayerCamera.frame = self.getPhotoButton.layer.bounds;
    
    gradientLayerCamera.colors = [NSArray arrayWithObjects:
                                  (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
                                  (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
                                  nil];
    
    gradientLayerCamera.locations = [NSArray arrayWithObjects:
                                     [NSNumber numberWithFloat:0.0f],
                                     [NSNumber numberWithFloat:1.0f],
                                     nil];
    
    self.getCameraButton.layer.cornerRadius = 15.0;
    gradientLayerCamera.cornerRadius = self.getCameraButton.layer.cornerRadius;
    [self.getCameraButton setContentMode:UIViewContentModeCenter];
    self.getCameraButton.clipsToBounds = YES;
    [self.getCameraButton.layer addSublayer:gradientLayerCamera];
    
    [self.manager getCoupleForUser];
}

-(void)viewWillAppear:(BOOL)animated{
    NSString *userImageUrl = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"imageProfile"];
    NSString *userImageUrlFacebook = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"imageProfile"];
    if ([userImageUrl isKindOfClass:[UIImage class]]) {
        [self.emptyView removeFromSuperview];
        self.imageView.image = (UIImage *)userImageUrl;
        self.imageView.layer.cornerRadius = 30;
        self.imageView.clipsToBounds = YES;
        [self.view addSubview: self.imageView];
    }else{
        if (userImageUrl.length >0 && ![userImageUrl isEqualToString:@"(null)"]) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImageUrl]];
            [self.emptyView removeFromSuperview];
            self.imageView.image = [UIImage imageWithData:imageData];
            self.imageView.layer.cornerRadius = 30;
            self.imageView.clipsToBounds = YES;
            [self.view addSubview: self.imageView];
        }else if(userImageUrlFacebook){
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImageUrlFacebook]];
            [self.emptyView removeFromSuperview];
            self.imageView.image = [UIImage imageWithData:imageData];
            self.imageView.layer.cornerRadius = 30;
            self.imageView.clipsToBounds = YES;
            [self.view addSubview: self.imageView];
            
        }else{
            [self.emptyView removeFromSuperview];
            self.imageView.image = [UIImage imageNamed:@"LocationTestLogo"];
            self.imageView.layer.cornerRadius = 30;
            self.imageView.clipsToBounds = YES;
            [self.view addSubview: self.imageView];
        }
        
    }

}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setCoupleTextfieldWithText:(NSString *)text{
    self.coupleTextfield.text = text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)saveButtonPressed:(id)sender {
    
    [self.manager updateUserWithUsername:self.usernameTextfield.text Couple:self.coupleDict Email:self.emailTextfield.text];
    
}
- (IBAction)cancelButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)getPhotoButtonPressed:(id)sender {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)getCameraButton:(id)sender {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }
}

- (IBAction)logOutButtonPressed:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Do you want to log out?"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Yes"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                                   [PFUser logOut]; // Log out
                                   [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserApp"];
                                   [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserFacebook"];
                                   [[NSUserDefaults standardUserDefaults]synchronize];
                                   
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"No"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)getUserCouple:(id)sender{
    
    UITextField *couple = (UITextField *)sender;
    if ([self.coupleDict objectForKey:@"objectId"]) {
        [self.manager deleteFirstCoupleRelationWithId:[self.coupleDict objectForKey:@"objectId"]];
        [self.manager deleteSecondCoupleRelationWithId:[self.coupleDict objectForKey:@"objectId"]];
    }
    [self.manager checkUserForAddCouple:couple.text WithFlag:YES];
    
}

#pragma mark RequestManager Delegate
-(void)didReceivedUserForCouple:(PFObject *)user{
    
    if (user) {
        PFUser *couple = (PFUser *)user;
        PFFile *imageFile = [couple objectForKey:@"image"];
        NSMutableDictionary *coupleDict = [[NSMutableDictionary alloc]init];
        
        [coupleDict setObject:couple.objectId forKey:@"objectId"];
        [coupleDict setObject:couple.username forKey:@"username"];
        [coupleDict setObject:couple.email forKey:@"email"];
        [coupleDict setObject:imageFile.url forKey:@"imageProfile"];
        
        self.coupleDict = coupleDict;
        NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length > 0) {
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]setObject:coupleDict forKey:@"couple"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]setObject:coupleDict forKey:@"couple"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        [self setCoupleTextfieldWithText:[coupleDict objectForKey:@"username"]];
        [self.manager saveCoupleRelationWithId:[coupleDict objectForKey:@"objectId"]];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"This user does not exist"
                                                              message:@"Try Again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
}

-(void)didUpdateUser:(BOOL)succeeded{
    if (succeeded) {
        NSLog(@"did update user");
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Could not update your profile"
                                                              message:@"Try Again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
}

-(void)didGetCouple:(NSString *)couple{
    if (couple.length) {
        [self.manager getUserWithId:couple];
    }
}

-(void)didReceivedUser:(PFObject *)user{
    
    if (user) {
        PFUser *couple = (PFUser *)user;
        PFFile *imageFile = [couple objectForKey:@"image"];
        NSMutableDictionary *coupleDict = [[NSMutableDictionary alloc]init];
        
        [coupleDict setObject:couple.objectId forKey:@"objectId"];
        [coupleDict setObject:couple.username forKey:@"username"];
        [coupleDict setObject:couple.email forKey:@"email"];
        [coupleDict setObject:imageFile.url forKey:@"imageProfile"];
        
        self.coupleDict = coupleDict;
        NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length > 0) {
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]setObject:coupleDict forKey:@"couple"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]setObject:coupleDict forKey:@"couple"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        [self setCoupleTextfieldWithText:[coupleDict objectForKey:@"username"]];

    }
}

-(void)hasNoConnection{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Device has no internet connection"
                                          message:@"Please connect device to internet"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    if (userName.length > 0) {
        [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:chosenImage forKey:@"imageProfile"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    self.imageView.alpha = 1;
    self.imageView.image = chosenImage;
    self.imageView.layer.cornerRadius = 10;
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        self.view.frame = CGRectMake(0, self.view.frame.size.height/5 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.view.frame = CGRectMake(0, self.view.frame.size.height/3 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.usernameTextfield) {
        [self.coupleTextfield becomeFirstResponder];
    }else if (textField == self.coupleTextfield){
        [self.emailTextfield becomeFirstResponder];
    }else{
        [self.view endEditing:YES];
    }
    
    return YES;
}
@end
