//
//  SettingsViewController.h
//  LocationTest
//
//  Created by Oriol Orra Serra on 13/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SetingsViewControllerDelegate

- (void)didChangeMapType:(NSInteger)type;
- (void)didChangeFilter:(NSInteger)filter;
- (void)didPressedLocationsList;
- (void)didPressedFriendsList;
- (void)didPressedProfileUser;

@end

@interface SettingsViewController : UIViewController

@property (nonatomic) NSInteger selectedMapType;
@property (nonatomic) NSInteger selectedFilter;
@property (nonatomic,strong) id <SetingsViewControllerDelegate> delegate;

@end
