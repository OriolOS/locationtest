//
//  ProfileUserViewController.h
//  LocationTest
//
//  Created by Winparf on 16/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileUserViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *getPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *getCameraButton;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *coupleTextfield;
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UIButton *logOutButton;

@end
