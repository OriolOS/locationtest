//
//  SettingsViewController.m
//  LocationTest
//
//  Created by Oriol Orra Serra on 13/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong) UISegmentedControl *mapTypeSC;
@property (nonatomic,strong) UISegmentedControl *filterSC;
@property (nonatomic,strong) UIButton *locationsButton;
@property (nonatomic,strong) UIButton *friendsButton;
@property (nonatomic,strong) UIButton *profileButton;

@end

@implementation SettingsViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *mapTypeArray = [NSArray arrayWithObjects: @"Standard", @"Satellite", @"Hybrid", nil];
    self.mapTypeSC = [[UISegmentedControl alloc]initWithItems:mapTypeArray];
    self.mapTypeSC.selectedSegmentIndex = self.selectedMapType;
    [self.mapTypeSC addTarget:self action:@selector(segmentedControlMapTypePressed) forControlEvents:UIControlEventValueChanged];
    
    NSArray *filterArray = [NSArray arrayWithObjects: @"You", @"Couple", @"Friends", @"Public", nil];
    self.filterSC = [[UISegmentedControl alloc] initWithItems:filterArray];
    self.filterSC.selectedSegmentIndex = self.selectedFilter;
    [self.filterSC addTarget:self action:@selector(segmentedControlFilterPressed) forControlEvents:UIControlEventValueChanged];
    
    self.locationsButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.locationsButton addTarget:self action:@selector(locationsButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    self.friendsButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.friendsButton  addTarget:self action:@selector(friendsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.profileButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.profileButton  addTarget:self action:@selector(profileButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : self.view.tintColor};
    
    self.navigationItem.title = @"Settings";
    
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) segmentedControlMapTypePressed{
    self.selectedMapType = self.mapTypeSC.selectedSegmentIndex;
    [self.delegate didChangeMapType:self.mapTypeSC.selectedSegmentIndex];
}

- (void) segmentedControlFilterPressed{
    self.selectedFilter = self.filterSC.selectedSegmentIndex,
    [self.delegate didChangeFilter: self.filterSC.selectedSegmentIndex];
}

-(void)locationsButtonPressed{
    [self.delegate didPressedLocationsList];

}

-(void)friendsButtonPressed{
    [self.delegate didPressedFriendsList];
}

-(void)profileButtonPressed{
    [self.delegate didPressedProfileUser];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    switch (indexPath.row) {
            
        case 0:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"mapTypesCell"];
            self.mapTypeSC.frame = CGRectMake(25, 8, 200, 20);
            [[cell contentView] addSubview:self.mapTypeSC];
            break;
        case 1:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"separateCell"];
            cell.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.4];
            break;
        case 2:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"filterCell"];
            self.filterSC.frame = CGRectMake(25, 8, 200, 20);
            [[cell contentView] addSubview:self.filterSC];
            break;
        case 3:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"locationsCell"];
            self.locationsButton.frame = CGRectMake(0, 0, 250, 43);
            [self.locationsButton setTitle:@"Locations" forState:UIControlStateNormal];
            [[cell contentView] addSubview:self.locationsButton];
            break;
        case 4:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"friendsCell"];
            self.friendsButton.frame = CGRectMake(0, 0, 250, 43);
            self.friendsButton.frame = CGRectMake(0, 0, 250, 43);
            [self.friendsButton setTitle:@"Couple & Friends" forState:UIControlStateNormal];
            [[cell contentView] addSubview:self.friendsButton];
            break;
        case 5:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"profileCell"];
            self.profileButton.frame = CGRectMake(0, 0, 250, 43);
            self.profileButton.frame = CGRectMake(0, 0, 250, 43);
            [self.profileButton setTitle:@"Profile" forState:UIControlStateNormal];
            [[cell contentView] addSubview:self.profileButton];
            break;
        default:
            break;
    }
    
    return cell;


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
            
        case 0:
            return 35.0f;
            break;
        case 1:
            return 10.0f;
            break;
        case 2:
            return 35.0f;
            break;
        case 3:
            return 44.0f;
            break;
        case 4:
            return 44.0f;
            break;
        case 5:
            return 44.0f;
            break;
            
        default:
            return 44.0f;
            break;
    }
}
- (BOOL)shouldAutorotate
{
    
    
    return YES;
}


@end
