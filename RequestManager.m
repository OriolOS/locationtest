//
//  RequestManager.m
//  LocationTest
//
//  Created by Winparf on 15/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "RequestManager.h"
#import "ParseErrorHandlingController.h"
#import "MyAnnotation.h"

@implementation RequestManager

#pragma mark USERS QUERIES

- (void)updateUserWithUsername:(NSString *)username Couple:(NSDictionary *)couple Email:(NSString *)email{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        
        PFUser *currentUser = [PFUser currentUser];
        PFFile *imageFile;
        NSString *userImage;
        
        NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length > 0) {
            userImage = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"imageProfile"];
        }else{
            userImage = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]objectForKey:@"imageProfile"];
        }
        if ([userImage isKindOfClass:[UIImage class]]) {
            NSData *imageData = UIImageJPEGRepresentation((UIImage *)userImage, 0);
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
            [currentUser setObject:imageFile forKey:@"image"];
        }else if (userImage && ![userImage isEqualToString:@"(null)"]){
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImage]];
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
        }else{
            NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"LocationTestLogo"], 0);
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
        }
        
        [currentUser setObject:imageFile forKey:@"image"];
        [currentUser setObject:username forKey:@"username"];
        [currentUser setObject:email forKey:@"email"];
        
        [currentUser pinInBackground];
        
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                // The currentUser saved successfully.
                NSLog(@"Update user succeeded");
                [currentUser unpinInBackground];
                NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
                if (userName.length > 0) {
                    
                    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:username forKey:@"username"];
                    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:email forKey:@"email"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                }else{
                    
                    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]setObject:username forKey:@"username"];
                    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]setObject:email forKey:@"email"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            } else {
                // There was an error saving the currentUser.
                [ParseErrorHandlingController handleParseError:error];
            }
            [self.delegate didUpdateUser:succeeded];
        }];
    }else{
        [self.delegate hasNoConnection];
    }
}

-(void)setDefaultImageProfile:(NSString *) objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                PFFile *imageFile;
                NSString *facebookProfileImage = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]objectForKey:@"imageProfile"];
                if(facebookProfileImage.length > 0){
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:facebookProfileImage]];
                    NSString *filename = @"imageProfile";
                    imageFile = [PFFile fileWithName:filename data:imageData];
                }else{
                    NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"LocationTestLogo"], 0);
                    NSString *filename = @"imageProfile";
                    imageFile = [PFFile fileWithName:filename data:imageData];
                }
                
                [user setObject:imageFile forKey:@"image"];
                
                [user pinInBackground];
                
                [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (succeeded) {
                        [user unpinInBackground];
                        [self.delegate didSetDefaultImage:succeeded];
                    }else{
                        [ParseErrorHandlingController handleParseError:error];
                    }
                    
                }];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getUserWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedUser:user];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}


#pragma mark ANNOTATIONS QUERIES

-(void)getYourLocations{
    NSMutableArray *locationsArray = [[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"objectId"];
        if (!objectId.length > 0) {
            
            objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"objectId"];
        }
        
        PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
        [query whereKey:@"owner" equalTo:currentUser.objectId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if(!error){
                [locationsArray removeAllObjects];
                for (PFObject *object in results) {
                    
                    PFFile *imageFile = object[@"Photo"];
                    
                    PFGeoPoint *currentPoint = object[@"GeoPoint"];
                    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPoint.latitude, currentPoint.longitude);
                    MyAnnotation *annotation = [[MyAnnotation alloc]init];
                    
                    annotation.title = object[@"title"];
                    annotation.subtitle = object[@"text"];
                    annotation.coordinate = coordinate;
                    annotation.objectId = object.objectId;
                    annotation.filter = object[@"filter"];
                    annotation.pinImageIndex = object[@"PinImageIndex"];
                    annotation.photo =[UIImage imageWithData:[imageFile getData]];
                    //annotation.photoURL = imageFile.url;
                    
                    if ([annotation.filter isEqualToString:@"You"]) {
                        annotation.filterIndex = 0;
                    }else if ([annotation.filter isEqualToString:@"Couple"]) {
                        annotation.filterIndex = 1;
                    }else if ([annotation.filter isEqualToString:@"Friends"]) {
                        annotation.filterIndex = 2;
                    }else{
                        annotation.filterIndex = 3;
                    }
                    
                    [locationsArray addObject:annotation];
                }
                [self.delegate didReceivedYourLocations:locationsArray];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }

}

-(void)getAllLocationsWithFilter: (NSString *)filterString{
    NSMutableArray *locationsArray = [[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    // OK
    
    if ([filterString isEqualToString:@"Public"]) {
        if (internetStatus != NotReachable) {
            PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
            [query whereKey:@"filter" equalTo:filterString];
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if(!error){
                    [locationsArray removeAllObjects];
                    for (PFObject *object in results) {
                        
                        PFFile *imageFilePhoto = object[@"Photo"];
                        PFGeoPoint *currentPoint = object[@"GeoPoint"];
                        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPoint.latitude, currentPoint.longitude);
                        MyAnnotation *annotation = [[MyAnnotation alloc]init];
                        
                        annotation.title = object[@"title"];
                        annotation.subtitle = object[@"text"];
                        annotation.coordinate = coordinate;
                        annotation.objectId = object.objectId;
                        annotation.filter = object[@"filter"];
                        annotation.pinImageIndex = object[@"PinImageIndex"];
                        annotation.photo =[UIImage imageWithData:[imageFilePhoto getData]];
                        //annotation.photoURL = imageFilePhoto.url;
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat = @"d.M.yyyy";
                        annotation.dateString = [formatter stringFromDate:object.createdAt];
                        

                        
                        if ([annotation.filter isEqualToString:@"You"]) {
                            annotation.filterIndex = 0;
                        }else if ([annotation.filter isEqualToString:@"Couple"]) {
                            annotation.filterIndex = 1;
                        }else if ([annotation.filter isEqualToString:@"Friends"]) {
                            annotation.filterIndex = 2;
                        }else{
                            annotation.filterIndex = 3;
                        }
                        
                        [locationsArray addObject:annotation];
                    }
                    [self.delegate didReceivedAllLocations:locationsArray];
                }else{
                    [ParseErrorHandlingController handleParseError:error];
                }
                
            }];
        }else{
            [self.delegate hasNoConnection];
            
        }
    } else if ([filterString isEqualToString:@"You"]){
        if (internetStatus != NotReachable) {
            PFUser *currentUser = [PFUser currentUser];
            NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"objectId"];
            if (!objectId.length > 0) {
                
                objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"objectId"];
            }
            
            PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
            [query whereKey:@"owner" equalTo:currentUser.objectId];
            [query whereKey:@"filter" equalTo:filterString];
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if(!error){
                    [locationsArray removeAllObjects];
                    for (PFObject *object in results) {
                        
                        PFFile *imageFile = object[@"Photo"];
                        
                        PFGeoPoint *currentPoint = object[@"GeoPoint"];
                        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPoint.latitude, currentPoint.longitude);
                        MyAnnotation *annotation = [[MyAnnotation alloc]init];
                        
                        annotation.title = object[@"title"];
                        annotation.subtitle = object[@"text"];
                        annotation.coordinate = coordinate;
                        annotation.objectId = object.objectId;
                        annotation.filter = object[@"filter"];
                        annotation.pinImageIndex = object[@"PinImageIndex"];
                        annotation.photo =[UIImage imageWithData:[imageFile getData]];
                        //annotation.photoURL = imageFile.url;
                        
                        if ([annotation.filter isEqualToString:@"You"]) {
                            annotation.filterIndex = 0;
                        }else if ([annotation.filter isEqualToString:@"Couple"]) {
                            annotation.filterIndex = 1;
                        }else if ([annotation.filter isEqualToString:@"Friends"]) {
                            annotation.filterIndex = 2;
                        }else{
                            annotation.filterIndex = 3;
                        }
                        
                        [locationsArray addObject:annotation];
                    }
                    [self.delegate didReceivedAllLocations:locationsArray];
                }else{
                    [ParseErrorHandlingController handleParseError:error];
                }
                
            }];
        }else{
            [self.delegate hasNoConnection];
            
        }
    }else if ([filterString isEqualToString:@"Friends"]){
        if (internetStatus != NotReachable) {
            PFUser *currentUser = [PFUser currentUser];
            NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"objectId"];
            NSMutableArray *friends = [NSMutableArray new];
            [friends addObjectsFromArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"friends"]];

            if (!objectId) {
                [friends  addObjectsFromArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"friends"]];

            }

            [friends addObject:currentUser.objectId];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner IN %@", friends];
            PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations" predicate:predicate];
            [query whereKey:@"filter" equalTo:filterString];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if(!error){
                    [locationsArray removeAllObjects];
                    for (PFObject *object in results) {
                        
                        PFFile *imageFile = object[@"Photo"];
                        
                        PFGeoPoint *currentPoint = object[@"GeoPoint"];
                        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPoint.latitude, currentPoint.longitude);
                        MyAnnotation *annotation = [[MyAnnotation alloc]init];
                        
                        annotation.title = object[@"title"];
                        annotation.subtitle = object[@"text"];
                        annotation.coordinate = coordinate;
                        annotation.objectId = object.objectId;
                        annotation.filter = object[@"filter"];
                        annotation.pinImageIndex = object[@"PinImageIndex"];
                        annotation.photo =[UIImage imageWithData:[imageFile getData]];
                        annotation.photoURL = imageFile.url;
                        
                        if ([annotation.filter isEqualToString:@"You"]) {
                            annotation.filterIndex = 0;
                        }else if ([annotation.filter isEqualToString:@"Couple"]) {
                            annotation.filterIndex = 1;
                        }else if ([annotation.filter isEqualToString:@"Friends"]) {
                            annotation.filterIndex = 2;
                        }else{
                            annotation.filterIndex = 3;
                        }
                        
                        [locationsArray addObject:annotation];
                    }
                    [self.delegate didReceivedAllLocations:locationsArray];
                }else{
                    [ParseErrorHandlingController handleParseError:error];
                }
                
            }];
        }else{
            [self.delegate hasNoConnection];
            
        }
    }else if ([filterString isEqualToString:@"Couple"]){
        if (internetStatus != NotReachable) {
            PFUser *currentUser = [PFUser currentUser];
            NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"objectId"];
            NSDictionary *couple = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]valueForKey:@"couple"];
            if (!objectId) {
                
                objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"objectId"];
                couple = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]valueForKey:@"couple"];
            }
            
            NSMutableArray *filterArray = [NSMutableArray new];
            if ([couple objectForKey:@"objectId"]) {
                [filterArray addObject:[couple objectForKey:@"objectId"]];
            }
            [filterArray addObject:currentUser.objectId];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner IN %@", filterArray];
            PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations" predicate:predicate];
            [query whereKey:@"filter" equalTo:filterString];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if(!error){
                    [locationsArray removeAllObjects];
                    for (PFObject *object in results) {
                        
                        PFFile *imageFile = object[@"Photo"];

                        PFGeoPoint *currentPoint = object[@"GeoPoint"];
                        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPoint.latitude, currentPoint.longitude);
                        MyAnnotation *annotation = [[MyAnnotation alloc]init];
                        
                        annotation.title = object[@"title"];
                        annotation.subtitle = object[@"text"];
                        annotation.coordinate = coordinate;
                        annotation.objectId = object.objectId;
                        annotation.filter = object[@"filter"];
                        annotation.photo =[UIImage imageWithData:[imageFile getData]];
                        annotation.pinImageIndex = object[@"PinImageIndex"];
                        annotation.photoURL = imageFile.url;
                        
                        if ([annotation.filter isEqualToString:@"You"]) {
                            annotation.filterIndex = 0;
                        }else if ([annotation.filter isEqualToString:@"Couple"]) {
                            annotation.filterIndex = 1;
                        }else if ([annotation.filter isEqualToString:@"Friends"]) {
                            annotation.filterIndex = 2;
                        }else{
                            annotation.filterIndex = 3;
                        }
                        
                        [locationsArray addObject:annotation];
                    }
                    [self.delegate didReceivedAllLocations:locationsArray];
                }else{
                    [ParseErrorHandlingController handleParseError:error];
                }
            }];
        }else{
            [self.delegate hasNoConnection];
        }
    }
}

-(void)numberOfLocationsForUser{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];

    if (internetStatus != NotReachable) {
        
        PFUser *currentUser = [PFUser currentUser];
        PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
        [query whereKey:@"owner" equalTo:currentUser.objectId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if(!error){
                if (results.count) {
                    [self.delegate didGetNumberOfLocations:results.count];
                }else{
                    [self.delegate didGetNumberOfLocations:0];
                }
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
         }];
    }else{
        [self.delegate hasNoConnection];        
    }
}

-(void)saveLocation:(MyAnnotation *)newAnnotation{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable) {
        PFGeoPoint *currentPoint = [PFGeoPoint geoPointWithLatitude:newAnnotation.coordinate.latitude longitude:newAnnotation.coordinate.longitude];
        
        
        // Create a PFObject using the Post class and set the values we extracted above
        PFObject *postObject = [PFObject objectWithClassName:@"UsersAnnotations"];
        PFFile *imageFilePhoto;
        if (newAnnotation.photo) {
            NSData *imageData = UIImageJPEGRepresentation(newAnnotation.photo, 0);
            NSString *filename = newAnnotation.photo.accessibilityIdentifier;
            imageFilePhoto = [PFFile fileWithName:filename data:imageData];
            postObject[@"Photo"] = imageFilePhoto;
        }else{
            imageFilePhoto = nil;
        }
        [imageFilePhoto save];
        
        PFACL *acl=[PFACL ACL];
        [acl setPublicReadAccess:YES];
        [acl setPublicWriteAccess:YES];
        
        postObject[@"title"] = newAnnotation.title;
        postObject[@"text"] = newAnnotation.subtitle;
        postObject[@"filter"] =newAnnotation.filter;
        postObject[@"owner"] =newAnnotation.owner;
        postObject[@"GeoPoint"] = currentPoint;
        postObject[@"PinImageIndex"] = newAnnotation.pinImageIndex;
        postObject.ACL = acl;
        
        [postObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {  // Failed to save, show an alert view with the error message
                
                [ParseErrorHandlingController handleParseError:error];
            }
            if (succeeded) {  // Successfully saved, post a notification to tell other view controllersƒ
                NSLog(@"Save succeeded");
                newAnnotation.objectId = postObject.objectId;
                [postObject unpinInBackground];
                [self.delegate didSavedLocation:newAnnotation];
            } else {
                NSLog(@"Failed to save.");
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

//        [postObject pinInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
//            if (succeeded) {
//                [postObject saveEventually:^(BOOL succeeded, NSError *error) {
//                    if (error) {  // Failed to save, show an alert view with the error message
//                        
//                        [ParseErrorHandlingController handleParseError:error];
//                    }
//                    if (succeeded) {  // Successfully saved, post a notification to tell other view controllersƒ
//                        NSLog(@"Save succeeded");
//                        newAnnotation.objectId = postObject.objectId;
//                        [postObject unpinInBackground];
//                        [self.delegate didSavedLocation:newAnnotation];
//                    } else {
//                        NSLog(@"Failed to save.");
//                    }
//                }];
//            }else{
//                [self.delegate hasNoConnection];
//                
//            }
//        }];
//    }
//}

-(void)updateLocation:(MyAnnotation *)annotation{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFGeoPoint *currentPoint = [PFGeoPoint geoPointWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
        [query getObjectInBackgroundWithId:annotation.objectId block:^(PFObject *object, NSError *error) {
            if (!error) {
                
                PFFile *imageFilePhoto;
                if (annotation.photo) {
                    NSData *imageData = UIImageJPEGRepresentation(annotation.photo, 0);
                    NSString *filename = annotation.photo.accessibilityIdentifier;
                    imageFilePhoto = [PFFile fileWithName:filename data:imageData];
                    [object setObject:imageFilePhoto forKey:@"Photo"];
                }else{
                    imageFilePhoto = nil;
                }
                
                [object setObject:annotation.title forKey:@"title"];
                [object setObject:annotation.subtitle forKey:@"text"];
                [object setObject:annotation.filter forKey:@"filter"];
                [object setObject:annotation.owner forKey:@"owner"];
                [object setObject:currentPoint forKey:@"GeoPoint"];
                [object setObject:annotation.title forKey:@"title"];
                [object setObject:annotation.pinImageIndex forKey:@"PinImageIndex"];
                
                // Save
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(succeeded){
                        [self.delegate didUpdatedLocation:annotation];
                    }else if (error){
                        NSLog(@"Error: %@", error);
                        [ParseErrorHandlingController handleParseError:error];
                    }
                }];
                
            } else {
                // Did not find any UserStats for the current user
                NSLog(@"Error: %@", error);
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)deleteAnnotationWithObjectId:(MyAnnotation *)annotation{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFQuery queryWithClassName:@"UsersAnnotations"];
        [query whereKey:@"objectId" equalTo:annotation.objectId];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!object) {
                NSLog(@"The getFirstObject request failed.");
            } else {
                // The find succeeded.
                NSLog(@"Successfully retrieved the object.");
                [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded && !error) {
                        [self.delegate didDeletedAnnotation:succeeded];
                    } else {
                        NSLog(@"error: %@", error);
                        [ParseErrorHandlingController handleParseError:error];
                    }
                }];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}


#pragma mark FRIENDS RELATIONS QUERIES

-(void) checkUserForAddFriend:(NSString *)userString{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        if([userString containsString:@"@"]){
            [query whereKey:@"email" equalTo:userString];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                if(!error){
                    [self.delegate didReceivedUserForFriend:user];
                    
                }else{
                    NSLog(@"error: %@", error);
                    [ParseErrorHandlingController handleParseError:error];
                }
            }];
        }else{
            
            [query whereKey:@"username" equalTo:userString];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                if(error){
                    [ParseErrorHandlingController handleParseError:error];
                }
                [self.delegate didReceivedUserForFriend:user];
                
            }];
        }
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getFriendById:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query getObjectInBackgroundWithId:objectId block:^(PFObject *user, NSError *error){
            if(!error){
                [self.delegate didReceivedFriend:user];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

- (void)getFriendsForUser{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        
        PFQuery *query = [PFQuery queryWithClassName:@"FriendsRelations"];
        [query whereKey:@"currentUser" equalTo:currentUser.objectId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if(!error){
                NSMutableArray *userIds = [NSMutableArray new];
                for (PFObject *object in results) {
                    [userIds addObject:[object objectForKey:@"friend"]];
                }
                
                [self.delegate didGetFriendsForUser:userIds];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)saveFriendRelationWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        PFObject *postObject = [PFObject objectWithClassName:@"FriendsRelations"];
        PFObject *postObject1 = [PFObject objectWithClassName:@"FriendsRelations"];
        
        PFACL *acl=[PFACL ACL];
        [acl setPublicReadAccess:YES];
        [acl setPublicWriteAccess:YES];
        
        postObject[@"currentUser"] = currentUser.objectId;
        postObject[@"friend"] = objectId;
        postObject.ACL = acl;
        
        postObject1[@"currentUser"] = objectId;
        postObject1[@"friend"] = currentUser.objectId;
        postObject1.ACL = acl;
        
        [postObject saveInBackground];
        [postObject1 saveInBackground];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)deleteFirstFriendRelationWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFQuery queryWithClassName:@"FriendsRelations"];
        [query whereKey:@"currentUser" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [object deleteInBackground];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}
-(void)deleteSecondFriendRelationWithId:(NSString *)objectId{

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFQuery queryWithClassName:@"FriendsRelations"];
        [query whereKey:@"friend" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [object deleteInBackground];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}


#pragma mark COUPLE RELATION QUERIES

-(void) checkUserForAddCouple:(NSString *)userString WithFlag:(BOOL)isProfile{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        if (isProfile) {
            if([userString containsString:@"@"]){
                [query whereKey:@"email" equalTo:userString];
                [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                    if(!error){
                        [self.delegate didReceivedUserForFriend:user];
                        
                    }else{
                        NSLog(@"error: %@", error);
                        [ParseErrorHandlingController handleParseError:error];
                    }
                }];
            }else{
                [query whereKey:@"username" equalTo:userString];
                [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                    if(!error){
                        [self.delegate didReceivedUserForCouple:user];
                        
                    }else{
                        NSLog(@"error: %@", error);
                        [ParseErrorHandlingController handleParseError:error];
                    }
                }];
                
            }
        }else{
            
            [query whereKey:@"objectId" equalTo:userString];
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                if (!error) {
                    [self.delegate didReceivedUserForCouple:user];
                }else{
                    [ParseErrorHandlingController handleParseError:error];
                }
            }];
        }
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)saveCoupleRelationWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        PFObject *postObject = [PFObject objectWithClassName:@"CouplesRelations"];
        PFObject *postObject1 = [PFObject objectWithClassName:@"CouplesRelations"];
        
        PFACL *acl=[PFACL ACL];
        [acl setPublicReadAccess:YES];
        [acl setPublicWriteAccess:YES];
        
        postObject[@"partner1"] = currentUser.objectId;
        postObject[@"partner2"] = objectId;
        postObject.ACL = acl;
        
        postObject1[@"partner1"] = objectId;
        postObject1[@"partner2"] = currentUser.objectId;
        postObject1.ACL = acl;
        
        [postObject saveInBackground];
        [postObject1 saveInBackground];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

- (void)getCoupleForUser{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        
        PFQuery *query = [PFQuery queryWithClassName:@"CouplesRelations"];
        [query whereKey:@"partner1" equalTo:currentUser.objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if(!error){
                if (object) {
                    [self.delegate didGetCouple:[object objectForKey:@"partner2"]];
                }else{
                    [self.delegate didGetCouple:@""];
                }
                
            }else{
                [self.delegate didGetCouple:@""];
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)deleteFirstCoupleRelationWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFQuery queryWithClassName:@"CouplesRelations"];
        [query whereKey:@"partner1" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [object deleteInBackground];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}
-(void)deleteSecondCoupleRelationWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFQuery queryWithClassName:@"CouplesRelations"];
        [query whereKey:@"partner2" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [object deleteInBackground];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];

    }
}
@end
