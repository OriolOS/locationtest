//
//  LocationsListViewController.h
//  LocationTest
//
//  Created by Oriol Orra Serra on 14/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LocationsListViewControllerDelegate
- (void)modifiedLocationList:(NSMutableArray *)locationsArray;
@end

@interface LocationsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *locationsArray;
@property (nonatomic,strong) id <LocationsListViewControllerDelegate> delegate;

@end
