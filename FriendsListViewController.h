//
//  FriendsListViewController.h
//  LocationTest
//
//  Created by Oriol Orra Serra on 14/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *friendsList;
@property (nonatomic,strong) NSMutableArray *friendsListForCheck;
@property (nonatomic,strong) NSMutableArray *possibleFriends;
@end
