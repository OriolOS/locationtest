//
//  MyAnnotation.h
//  LocationTest
//
//  Created by Winparf on 15/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnotation : MKPointAnnotation
@property (nonatomic,strong) MKPointAnnotation *annotation;
@property (nonatomic,strong) NSString *filter;
@property (nonatomic) NSInteger filterIndex;
@property (nonatomic,strong) NSString *objectId;
@property (nonatomic,strong) NSString *owner;
@property (nonatomic,strong) UIImage *photo;
@property (nonatomic,strong) NSString *photoURL;
@property (nonatomic,strong) NSString *dateString;
@property (nonatomic) NSNumber *pinImageIndex;
@end
