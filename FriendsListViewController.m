//
//  FriendsListViewController.m
//  LocationTest
//
//  Created by Oriol Orra Serra on 14/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//
@import GoogleMobileAds;

#import "FriendsListViewController.h"
#import "RequestManager.h"

@interface FriendsListViewController () <UITableViewDataSource, UITableViewDelegate,RequestManagerDelegate>

@property (nonatomic,strong) RequestManager *manager;
@property (nonatomic,strong) UITextField *addFriendTextField;
@property (nonatomic,strong) NSDictionary *coupleDict;
@property (nonatomic,strong) PFUser *couple;
@property (nonatomic)NSUInteger friendsNumber;
@property (nonatomic)BOOL isCouple;
@property (nonatomic,strong)NSString *objectIdToDelete;

@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;

@end

@implementation FriendsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.manager = [[RequestManager alloc]init];
    self.manager.delegate = self;
    self.addFriendTextField = [[UITextField alloc]init];
    self.coupleDict = [[NSDictionary alloc]init];
    
    self.friendsNumber = 0;
    
    if(!_friendsList){
        _friendsList = [[NSMutableArray alloc]init];
    }else{
        [_friendsList removeAllObjects];
    }
    if (!_friendsListForCheck) {
        _friendsListForCheck = [[NSMutableArray alloc]init];
    }else{
        [_friendsListForCheck removeAllObjects];
    }
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    // Replace this ad unit ID with your own ad unit ID.
    [self.adBannerView setAdUnitID:@"ca-app-pub-8715619194809926/6780050496"];
    self.adBannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [self.adBannerView loadRequest:request];

}

-(void)viewWillAppear:(BOOL)animated{
    
//    NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
//    if (userName.length > 0) {
//        if([[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]objectForKey:@"friends"] isKindOfClass:[NSString class]]){
//            if (![[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]objectForKey:@"friends"] isEqualToString:@""]){
//                [self.friendsListForCheck removeAllObjects];
//            }
//        }else{
//            self.friendsListForCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]objectForKey:@"friends"];
//            self.coupleDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]objectForKey:@"couple"];
//
//            }
//    }else{
//        if([[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]objectForKey:@"friends"] isKindOfClass:[NSString class]]){
//            if (![[[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]objectForKey:@"friends"] isEqualToString:@""]) {
//                [self.friendsListForCheck removeAllObjects];
//            }
//        }else{
//            
//            self.friendsListForCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]objectForKey:@"friends"];
//            self.coupleDict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]objectForKey:@"couple"];
//
//        }
//    }
    
    self.friendsNumber = 0;
    [self.friendsListForCheck removeAllObjects];
    
    [self.manager getCoupleForUser];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setAddFriendTextFieldWith:(id) sender{
    
    UITextField *textfield = sender;
    self.addFriendTextField.text = textfield.text;
    
}
- (IBAction)addFriend:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Please enter an username or/and an email"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Friend's username or email";
         [textField addTarget:self action:@selector(setAddFriendTextFieldWith:) forControlEvents:UIControlEventEditingDidEnd];
         
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"ADD"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self.manager checkUserForAddFriend:self.addFriendTextField.text];
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];

    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)doneButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *title;
    if (section == 0) {
        title = @"Your partner life";
    }else if (section == 1){
        title = @"Friends";
    }
    return title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numberRows = 0;
    
    if (section == 0) {
        numberRows = 1;
    }
    if(section == 1){
        numberRows = self.friendsList.count;
    }
    
    return numberRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"friendCell"];
    if (indexPath.section == 0 && [self.coupleDict objectForKey:@"objectId"]) {

        NSString *userImageUrl;
        NSData *imageData;
        if ([[self.coupleDict objectForKey:@"imageProfile"] isKindOfClass:[NSString class]]) {
            userImageUrl = [self.coupleDict objectForKey:@"imageProfile"];
            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImageUrl]];
        }else if ([[self.coupleDict objectForKey:@"imageProfile"] isKindOfClass:[UIImage class]]){
            imageData = UIImagePNGRepresentation([self.coupleDict objectForKey:@"imageProfile"]);
        }else{
            imageData = nil;
        }
        
        NSString *title = [self.coupleDict objectForKey:@"username"];
        
        cell.textLabel.text = title;
        cell.detailTextLabel.text = [self.coupleDict objectForKey:@"email"];
        cell.detailTextLabel.clipsToBounds = YES;
        cell.imageView.image = [UIImage imageWithData:imageData];
        cell.imageView.layer.cornerRadius = 5;
        cell.imageView.clipsToBounds = YES;
    }else
        if(indexPath.section == 0){
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;

    }

    if (indexPath.section == 1) {
        
        PFUser *user = [self.friendsList objectAtIndex:indexPath.row];
        PFFile *imageFile;
        NSString *userImageUrl;
        NSData *imageData;
        if ([user valueForKey:@"image"]) {
            imageFile = [user objectForKey:@"image"];
            userImageUrl = [NSString stringWithFormat:@"%@",imageFile.url];
            imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImageUrl]];
        }else{
            imageData = nil;
        }
        
        NSString *title;
        if ([[user objectForKey:@"username"] length]) {
            title = [user objectForKey:@"username"];
        }else{
            title = [user objectForKey:@"name"];
        }
        
        cell.textLabel.text = title;
        cell.detailTextLabel.text = [user objectForKey:@"email"];
        cell.imageView.image = [UIImage imageWithData:imageData];
        cell.imageView.layer.cornerRadius = 5;
        cell.imageView.clipsToBounds = YES;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60.0;
}

#pragma mark UiTableView Delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return YES - we will be able to delete all rows
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0 ){
        
        [self.manager deleteFirstCoupleRelationWithId:[self.coupleDict objectForKey:@"objectId"]];
        [self.manager deleteSecondCoupleRelationWithId:[self.coupleDict objectForKey:@"objectId"]];
        self.coupleDict =  [NSDictionary new];
    }
    
    if (indexPath.section == 1) {
        self.objectIdToDelete = [self.friendsListForCheck objectAtIndex:indexPath.row];
        self.friendsNumber --;
        [self.friendsList removeObjectAtIndex:indexPath.row];
        [self.friendsListForCheck removeObjectAtIndex:indexPath.row];
        [self.manager deleteFirstFriendRelationWithId:self.objectIdToDelete];
        [self.manager deleteSecondFriendRelationWithId:self.objectIdToDelete];
    }

    
    [self.tableView reloadData];
    
}


#pragma mark RequestManager Delegate


-(void)didGetCouple:(NSString *)couple{
    if (couple.length) {
        [self.manager getUserWithId:couple];
    }else{
        [self.manager getFriendsForUser];
    }
}

-(void)didReceivedUser:(PFObject *)user{
    if (user) {
        PFUser *couple = (PFUser *)user;

        PFFile *imageFile = [couple objectForKey:@"image"];
        NSMutableDictionary *coupleDict = [[NSMutableDictionary alloc]init];
        
        [coupleDict setObject:couple.objectId forKey:@"objectId"];
        [coupleDict setObject:couple.username forKey:@"username"];
        [coupleDict setObject:couple.email forKey:@"email"];
        [coupleDict setObject:imageFile.url forKey:@"imageProfile"];
        
        self.coupleDict = coupleDict;
        [self.tableView reloadData];
    }
    
    [self.manager getFriendsForUser];
    
}

-(void)didGetFriendsForUser:(NSMutableArray *)userIds{
    
    if (userIds.count) {
        [self.friendsListForCheck addObjectsFromArray:userIds];
        
        NSString *objectId = [self.friendsListForCheck objectAtIndex:self.friendsNumber];
        self.friendsNumber ++;
        [self.manager getFriendById:objectId];
    }
}

-(void)didReceivedUserForFriend:(PFObject *)user{
    
    if (user) {
        
        [self.friendsListForCheck addObject:user.objectId];
        
        NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length > 0) {
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]setObject:self.friendsListForCheck forKey:@"friends"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]setObject:self.friendsListForCheck forKey:@"friends"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        [self.manager saveFriendRelationWithId:user.objectId];

        NSString *objectId = [self.friendsListForCheck objectAtIndex:self.friendsNumber];
        self.friendsNumber ++;
        [self.manager getFriendById:objectId];
        
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"This user does not exist"
                                                              message:@"Try Again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

-(void)didReceivedFriend:(PFObject *)user{
    if (user) {
        
        [self.friendsList addObject:user];
        
        if (self.friendsNumber < self.friendsListForCheck.count) {
            NSString *objectId = [self.friendsListForCheck objectAtIndex:self.friendsNumber];
            self.friendsNumber ++;
            [self.manager getFriendById:objectId];
        }
        [self.tableView reloadData];
        
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error while we were retreiving friend"
                                                              message:@""
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

-(void)hasNoConnection{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Device has no internet connection"
                                          message:@"Please connect device to internet"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];

}

@end
