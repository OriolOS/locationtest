//
//  ParseErrorHandlingController.m
//  LocationTest
//
//  Created by Winparf on 17/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "ParseErrorHandlingController.h"
#import "ViewController.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>


@implementation ParseErrorHandlingController

+ (void)handleParseError:(NSError *)error {
    if (![error.domain isEqualToString:PFParseErrorDomain]) {
        return;
    }
    
    switch (error.code) {
        case kPFErrorInvalidSessionToken: {
            [self _handleInvalidSessionTokenError];
            break;
        }
            // Other Parse API Errors that you want to explicitly handle.
    }
}

+ (void)_handleInvalidSessionTokenError {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"invalidSessionHandling" object:nil];
    
    //--------------------------------------
    // Option 1: Show a message asking the user to log out and log back in.
    //--------------------------------------
    // If the user needs to finish what they were doing, they have the opportunity to do so.
    //
//     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid Session"
//                                                         message:@"Session is no longer valid, please log out and log in again."
//                                                        delegate:self
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles:nil];
//     [alertView show];
    //--------------------------------------
    // Option #2: Show login screen so user can re-authenticate.
    //--------------------------------------
    // You may want this if the logout button is inaccessible in the UI.
    //
//    UIViewController *presentingViewController = ([[UIApplication sharedApplication] delegate]).window.rootViewController;
//    ViewController *rootVC = (ViewController *)presentingViewController;
//    rootVC.isHandlingInvaildSession = YES;
//    [presentingViewController presentViewController:rootVC animated:YES completion:nil];
    
//    PFLogInViewController *logInViewController = [self resetLogInViewController:presentingViewController];
//     [presentingViewController presentViewController:logInViewController animated:YES completion:nil];
}

//+(PFLogInViewController *)resetLogInViewController:(UIViewController *)presentingViewController{
//    
//    PFLogInViewController *logInVC = [[PFLogInViewController alloc]init];
//    
//    UIImageView *logo1 = [[UIImageView alloc]initWithFrame:CGRectMake(presentingViewController.view.frame.size.width/6, 20, presentingViewController.view.frame.size.width * 2/3, presentingViewController.view.frame.size.height *1/4)];
//    logo1.contentMode = UIViewContentModeScaleAspectFit;
//    logo1.image = [UIImage imageNamed:@"LocationTestLogo"];
//    UIImageView *logo2 = [[UIImageView alloc]initWithFrame:CGRectMake(presentingViewController.view.frame.size.width/6, 20, presentingViewController.view.frame.size.width * 2/3, presentingViewController.view.frame.size.height *1/4)];
//    logo2.contentMode = UIViewContentModeScaleAspectFit;
//    logo2.image = [UIImage imageNamed:@"LocationTestLogo"];
//    
//    logInVC.fields = (PFLogInFieldsUsernameAndPassword
//                      | PFLogInFieldsLogInButton
//                      | PFLogInFieldsPasswordForgotten
//                      | PFLogInFieldsFacebook
//                      | PFLogInFieldsSignUpButton
//                      | PFLogInFieldsDismissButton);
//    //    //Set event for facebook button
//    [logInVC.logInView.facebookButton addTarget:self action:@selector(_loginWithFacebook) forControlEvents:UIControlEventTouchUpInside];
//    [logInVC.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
//    [logInVC.logInView.logo setContentMode:UIViewContentModeScaleAspectFit];
//    [logInVC.logInView addSubview:logo1];
//   // [logInVC setDelegate:self];
//    PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc]init];
//  //  [signUpViewController setDelegate:self];
//    [logInVC setSignUpController:signUpViewController];
//    [logInVC.signUpController.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
//    [logInVC.signUpController.signUpView.logo setContentMode:UIViewContentModeScaleAspectFit];
//    [logInVC.signUpController.signUpView addSubview:logo2];
//  //  logInVC.signUpController.delegate = self;
//    
//    return logInVC;
//}
//
//#pragma mark Facebook API
//
//+ (void)_loginWithFacebook {
//    // Set permissions required from the facebook user account
//    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email"];
//    
//    // Login PFUser using Facebook
//    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
//        if (!user) {
//            NSLog(@"Uh oh. The user cancelled the Facebook login.");
//        } else if (user.isNew) {
//
//            NSLog(@"User signed up and logged in through Facebook!");
//        } else {
//
//            NSLog(@"User logged in through Facebook!");
//        }
//    }];
//}



@end

// In all API requests, call the global error handler, e.g.
//[[PFQuery queryWithClassName:@"Object"] findInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//    if (!error) {
//        // Query succeeded - continue your app logic here.
//    } else {
//        // Query failed - handle an error.
//        [ParseErrorHandlingController handleParseError:error];
//    }
//}];
