//
//  AnnotationViewController.h
//  LocationTest
//
//  Created by Winparf on 15/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAnnotation.h"

@protocol AnnotationViewDelegate
- (void)didSaveNewAnnotationWithImage:(UIImage *)image TitleAnnotation:(NSString *)title Subtitle:(NSString *)subtitle Filter:(NSInteger)filter Owner:(NSString *)owner PinImageIndex:(NSNumber *)pinImageIndex;
- (void)didUpdateAnnotation:(MyAnnotation *)annotation;
@end

@interface AnnotationViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *pinImageButton;
@property (weak, nonatomic) IBOutlet UIButton *getPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *getCameraButton;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *subtitleTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *filterSegmentedControl;
@property (nonatomic)BOOL canEdit;
@property(nonatomic,strong) MyAnnotation *annotation;
@property (nonatomic,strong) id <AnnotationViewDelegate> delegate;

@end

