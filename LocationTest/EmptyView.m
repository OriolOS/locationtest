//
//  EmptyView.m
//  WinParfMobile
//
//  Created by Albert Villanueva on 16/3/15.
//  Copyright (c) 2015 WinParfServices. All rights reserved.
//

#import "EmptyView.h"

@interface EmptyView ()

@property (nonatomic, strong) NSString *labelText;
@property (nonatomic, strong) UIImage *image;

@end

@implementation EmptyView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame mainText:(NSString *)labelText andImage:(UIImage *)image  {
   self = [super initWithFrame:frame];
    
    if (self) {
        _labelText = labelText;
        _image = image;
        [self createOutlets];

    }
    
    return self;
    
}

- (void)createOutlets{

    self.label = [[UILabel alloc]initWithFrame:self.frame];
    self.label.textColor = [UIColor blackColor];
    self.label.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    self.label.text = self.labelText;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.center = self.center;
    [self addSubview:self.label];
}

@end
