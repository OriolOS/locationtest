//
//  ViewController.h
//  LocationTest
//
//  Created by Winparf on 11/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@import GoogleMobileAds;
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <Parse/Parse.h>


@protocol ViewControllerDelegate

-(void)didReceivedMapType:(NSInteger *)mapType;

@end

@interface ViewController : UIViewController <MKMapViewDelegate,  CLLocationManagerDelegate>

@property (nonatomic)BOOL isHandlingInvaildSession;
@property (nonatomic, strong)UINavigationController *navigationController;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;
@property(nonatomic) MKCoordinateRegion currentRegion;
@property (weak, nonatomic) IBOutlet UIButton *zoomInButton;
@property (weak, nonatomic) IBOutlet UIButton *zoomOutButton;
@property (weak, nonatomic) IBOutlet UIButton *deviceLocationButton;

@property (nonatomic) int timeToIntersticial;
@property (nonatomic,strong) GADInterstitial *adIntersticialView;

@end

