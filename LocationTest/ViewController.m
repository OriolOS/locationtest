//
//  ViewController.m
//  LocationTest
//
//  Created by Winparf on 11/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <ParseUI/ParseUI.h>
#import "Reachability.h"
#import "WYPopoverController.h"
#import "SettingsViewController.h"
#import "LocationsListViewController.h"
#import "FriendsListViewController.h"
#import "ProfileUserViewController.h"
#import "AnnotationViewController.h"
#import "MyAnnotation.h"
#import "RequestManager.h"
#import <MapKit/MapKit.h>
#import "ViewController.h"

@interface ViewController () <WYPopoverControllerDelegate,SetingsViewControllerDelegate,MKMapViewDelegate,AnnotationViewDelegate, RequestManagerDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, LocationsListViewControllerDelegate,GADInterstitialDelegate>
{
    WYPopoverController *settingsPopoverController;
}
@property (nonatomic,strong)NSMutableArray *settingsArray;
@property (nonatomic,strong) SettingsViewController *settingsViewController;
@property (nonatomic) NSInteger selectedMapType;
@property (nonatomic) NSInteger selectedFilter;
@property (nonatomic) NSString *selectedFilterString;
@property (nonatomic,strong) UIImage *currentPhoto;
@property (nonatomic,strong) NSMutableArray *locationsArray;
@property (nonatomic,strong) UINavigationBar *navigationBar;
@property (nonatomic,strong) RequestManager *manager;
@property(nonatomic,strong) PFLogInViewController *logInViewController;
@property(nonatomic,strong) PFLogInView *logInView;
@property(nonatomic,strong) PFSignUpViewController *signUpViewController;
@property (nonatomic)BOOL rightButtonsPressed;
@property (nonatomic,strong) MyAnnotation *selectedAnnotation;
@property (nonatomic,strong) MyAnnotation *saveAnnotation;
@property (nonatomic,strong) NSMutableArray *pinImagesArray;

@property(nonatomic)BOOL isSignedUp;
@property (nonatomic) NSString *signedUpObjectId;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.manager = [[RequestManager alloc]init];
    self.manager.delegate = self;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didReceivedInvalidSession) name:@"invalidSessionHandling" object:nil];

    self.pinImagesArray = [[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"pinIcon"],[UIImage imageNamed:@"LocationIcon"],[UIImage imageNamed:@"AddLocation"],nil];
    
    self.selectedFilter = 3;
    self.selectedFilterString = @"Public";
    
    if (!_locationsArray) {
        _locationsArray = [[NSMutableArray alloc]init];
    }
    
    self.navigationBar = [self styleNavBar];
    
    [self.view addSubview:self.navigationBar];
    
    [self.zoomInButton addTarget:self action:@selector(zoomInButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.zoomOutButton addTarget:self action:@selector(zoomOutButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.deviceLocationButton addTarget:self action:@selector(deviceLocationButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.mapView addSubview:self.zoomInButton];
    [self.mapView addSubview:self.zoomOutButton];
    [self.mapView addSubview:self.deviceLocationButton];
    self.mapView.showsUserLocation = YES;
    self.mapView.zoomEnabled = YES;
    
    self.mapView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {

        [self.locationManager requestAlwaysAuthorization];
    }
#endif

    [self.locationManager startUpdatingLocation];
    
    self.mapView.showsUserLocation = YES;
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    
    //LOG IN LINES
    if (!_logInViewController) {
        
        _logInViewController = [self resetLogInViewController];
    }
    
    self.currentRegion = region;
    [self.mapView setRegion:self.currentRegion animated:YES];
    


}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"] && ![[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]) {
        [self.mapView removeAnnotations:self.locationsArray];
        [self.locationsArray removeAllObjects];
        self.logInViewController = [self resetLogInViewController];
        
        [self presentViewController:self.logInViewController animated:YES completion:NULL];
        
    }else{
        NSString *objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]objectForKey:@"objectId"];
        if (!objectId.length > 0) {
            objectId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]objectForKey:@"objectId"];
        }
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus != NotReachable) {
            
            NSLog(@"IS REACHABLE");
            [self.manager getCoupleForUser];
            [self.manager getAllLocationsWithFilter:self.selectedFilterString];
        }else {
            NSLog(@"NOT REACHABLE");
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Device has no internet connection"
                                                                  message:@"Please connect device to internet"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            [myAlertView show];
        }
    }
    
    if (self.isSignedUp) {
        [self.manager setDefaultImageProfile:self.signedUpObjectId];
        self.isSignedUp = NO;
    }
    
    //Load Add Interstitial
    if (self.timeToIntersticial >= 5) {
        
        [self setInterstitialAd];
        self.timeToIntersticial = 0;
    }
}

- (UINavigationBar *)styleNavBar {
    // 1. hide the existing nav bar
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    // 2. create a new nav bar and style it
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
    newNavBar.barStyle = UIBarStyleBlackTranslucent;
    newNavBar.alpha = 1.0;
    // 3. add a new navigation item w/title to the new nav bar
    UINavigationItem *newItem = [[UINavigationItem alloc] initWithTitle:@"Map"];
    
    NSMutableArray *leftBarButtons = [[NSMutableArray alloc]init];
    NSMutableArray *rightBarButtons = [[NSMutableArray alloc]init];
    
    UIButton *facebookShareButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [facebookShareButton setImage:[UIImage imageNamed:@"FacebookLogo.png"] forState:UIControlStateNormal];
    facebookShareButton.tintColor = [UIColor whiteColor];
    facebookShareButton.frame = CGRectMake(0, 0, 25, 25);
    facebookShareButton.showsTouchWhenHighlighted=YES;
    [facebookShareButton addTarget:self action:@selector(shareWithFacebook) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:facebookShareButton];
    [leftBarButtons addObject:barButtonItem2];
    
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [settingsButton setImage:[UIImage imageNamed:@"Settings"] forState:UIControlStateNormal];
    settingsButton.tintColor = [UIColor whiteColor];
    settingsButton.frame = CGRectMake(0, 0, 25, 25);
    settingsButton.showsTouchWhenHighlighted=YES;
    [settingsButton addTarget:self action:@selector(settingsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem3 = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    [rightBarButtons addObject:barButtonItem3];
    
    UIButton *addLocationButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [addLocationButton setImage:[UIImage imageNamed:@"AddLocation"] forState:UIControlStateNormal];
    addLocationButton.tintColor = [UIColor whiteColor];
    addLocationButton.frame = CGRectMake(0, 0, 25, 25);
    addLocationButton.showsTouchWhenHighlighted=YES;
    [addLocationButton addTarget:self action:@selector(addLocationButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem4 = [[UIBarButtonItem alloc] initWithCustomView:addLocationButton];
    [rightBarButtons addObject:barButtonItem4];
    
    newItem.leftBarButtonItems = leftBarButtons;
    newItem.rightBarButtonItems = rightBarButtons;
    
    newNavBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [newNavBar setItems:@[newItem]];
    
    // 4. add the nav bar to the main view
    return newNavBar;
}

-(PFLogInViewController *)resetLogInViewController{
    
    self.logInViewController = nil;
    
    PFLogInViewController *logInVC = [[PFLogInViewController alloc]init];
    
    UIImageView *logo1 = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 20, self.view.frame.size.width * 2/3, self.view.frame.size.height *1/4)];
    logo1.contentMode = UIViewContentModeScaleAspectFit;
    logo1.image = [UIImage imageNamed:@"LocationTestLogo"];
    UIImageView *logo2 = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 20, self.view.frame.size.width * 2/3, self.view.frame.size.height *1/4)];
    logo2.contentMode = UIViewContentModeScaleAspectFit;
    logo2.image = [UIImage imageNamed:@"LocationTestLogo"];
    
    logInVC.fields = (PFLogInFieldsUsernameAndPassword
                      | PFLogInFieldsLogInButton
                      | PFLogInFieldsPasswordForgotten
                      | PFLogInFieldsFacebook
                      | PFLogInFieldsSignUpButton
                      | PFLogInFieldsDismissButton);
//    //Set event for facebook button
    [logInVC.logInView.facebookButton addTarget:self action:@selector(_loginWithFacebook) forControlEvents:UIControlEventTouchUpInside];
    [logInVC.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
    [logInVC.logInView.logo setContentMode:UIViewContentModeScaleAspectFit];
    [logInVC.logInView addSubview:logo1];
    [logInVC setDelegate:self];
    self.signUpViewController = [[PFSignUpViewController alloc]init];
    [self.signUpViewController setDelegate:self];
    [logInVC setSignUpController:self.signUpViewController];
    [logInVC.signUpController.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
    [logInVC.signUpController.signUpView.logo setContentMode:UIViewContentModeScaleAspectFit];
    [logInVC.signUpController.signUpView addSubview:logo2];
    logInVC.signUpController.delegate = self;
    
    return logInVC;
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.altitude];
}

- (BOOL)shouldAutorotate
{
    self.navigationBar.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0);
    return YES;
}

-(void)didReceivedInvalidSession{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserApp"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserFacebook"];
    
    self.logInViewController = [self resetLogInViewController];
    [self presentViewController:self.logInViewController animated:YES completion:nil];
    
}


#pragma mark    Methods for UIButtons added in mapView
- (void)zoomInButtonPressed: (id)sender
{
    MKCoordinateRegion region = self.mapView.region;
    MKCoordinateSpan span;
    span.latitudeDelta = region.span.latitudeDelta/2;
    span.longitudeDelta = region.span.longitudeDelta/2;
    region.span = span;
    [self.mapView setRegion:region animated:TRUE];
}
- (void)zoomOutButtonPressed: (id)sender
{
    MKCoordinateRegion region = self.mapView.region;
    MKCoordinateSpan span;
    span.latitudeDelta = region.span.latitudeDelta*2;
    span.longitudeDelta = region.span.longitudeDelta*2;
    region.span = span;
    if (!(span.latitudeDelta >= 180.0 || span.longitudeDelta >= 180.0)) {
        region.span = span;
        [self.mapView setRegion:region animated:TRUE];
    }
}
-(void)deviceLocationButtonPressed: (id)sender{
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"button latitude: %f longitude: %f", self.currentRegion.center.latitude,self.currentRegion.center.longitude);
    
    //View Area
    [self.mapView setRegion:self.currentRegion animated:YES];
}

-(void)getLocationUser{
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"latitude: %f longitude: %f", self.currentRegion.center.latitude,self.currentRegion.center.longitude);
    
    //View Area
    [self.mapView setRegion:self.currentRegion animated:YES];
}



#pragma mark    Methods for UIBarButtons

-(void)logInButtonPressed{
    NSLog(@"Log In button pressed");
    self.logInViewController = [self resetLogInViewController];
    self.timeToIntersticial ++;
    [self presentViewController:self.logInViewController animated:YES completion:nil];
}

-(void)shareWithFacebook{
    NSLog(@"Share facebook button pressed");
    
    if([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]){
        
        self.timeToIntersticial ++;
        
        NSMutableArray *array = [NSMutableArray arrayWithObject:@"publish_actions"];
        [PFFacebookUtils logInInBackgroundWithPublishPermissions:array block:^(PFUser *user, NSError *error){
            
        }];
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:@"https://www.facebook.com/games/?fbs=-1&app_id=844893465581256&preview=1&locale=ca_ES"];
        [FBSDKShareDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];
        
        
    }else{
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"This user is not logged in Facebook"
                                              message:@"Please log with Facebook"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}

-(void)settingsButtonPressed:(id)sender{
    NSLog(@"Settings button pressed");
    self.timeToIntersticial ++;
    if (settingsPopoverController == nil){
        UIView * btn = (UIView*)sender;
        
         self.settingsViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"settingsVC"];
        self.settingsViewController.modalInPopover = NO;
        self.settingsViewController.delegate = self;
        self.settingsViewController.preferredContentSize = CGSizeMake(250, 258);
        
        self.settingsViewController.selectedMapType = self.selectedMapType;
        self.settingsViewController.selectedFilter = self.selectedFilter;

        
        UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:self.settingsViewController];
        
        settingsPopoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
        settingsPopoverController.delegate = self;
        settingsPopoverController.theme.outerCornerRadius = 15;
        settingsPopoverController.theme.innerCornerRadius = 15;
        settingsPopoverController.theme.fillTopColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.9];
        settingsPopoverController.theme.fillBottomColor = [UIColor clearColor];
        settingsPopoverController.passthroughViews = @[btn];
        settingsPopoverController.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
        settingsPopoverController.wantsDefaultContentAppearance = NO;
        
        [settingsPopoverController presentPopoverFromRect:btn.bounds
                                                   inView:btn
                                 permittedArrowDirections:WYPopoverArrowDirectionAny
                                                 animated:YES
                                                  options:WYPopoverAnimationOptionFadeWithScale];
        
    }else{
        [self done:nil];
    }

}

-(void)addLocationButtonPressed{
    NSLog(@"Add location button pressed");
    
    self.timeToIntersticial ++;
    UINavigationController *annotationNC = [self.storyboard instantiateViewControllerWithIdentifier:@"annotationNC"];
    AnnotationViewController *annotationVC = (AnnotationViewController *)annotationNC.topViewController;
    annotationVC.delegate = self;
    annotationVC.canEdit = YES;
    
    [self presentViewController:annotationNC animated:YES completion:nil];
    
}

#pragma mark WYPopoverController
- (void)done:(id)sender
{
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    controller.delegate = nil;
    controller = nil;
}


#pragma mark SettingsViewController delegate

-(void)didChangeMapType:(NSInteger)mapType{
    
    switch (mapType) {
        case 0:
            self.mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
    self.selectedMapType = mapType;
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
    
}

-(void)didChangeFilter:(NSInteger)filter{
 
    self.selectedFilter = filter;
    NSString * stringFilter;
    switch (filter) {
        case 0:
            stringFilter = @"You";
            break;
        case 1:
            stringFilter = @"Couple";
            break;
        case 2:
            stringFilter = @"Friends";
            break;
        case 3:
            stringFilter = @"Public";
            break;
            
        default:
            break;
    }
    [self.manager getAllLocationsWithFilter:stringFilter];
    
    self.selectedFilterString = stringFilter;
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
}
-(void)didPressedLocationsList{
    
    [self.manager getYourLocations];
}

-(void)didPressedFriendsList{
    
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
    
    
    UINavigationController  *friendsListNC = [self.storyboard instantiateViewControllerWithIdentifier:@"friendsListNC"];
    FriendsListViewController *friendsListVC = (FriendsListViewController *)friendsListNC.topViewController;
    
    [self presentViewController:friendsListNC animated:YES completion:nil];
    
}

-(void)didPressedProfileUser{
    
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
    
    UINavigationController  *profileNC = [self.storyboard instantiateViewControllerWithIdentifier:@"profileNC"];
     ProfileUserViewController *profileVC = (ProfileUserViewController *)profileNC.topViewController;
    
    
    
    [self presentViewController:profileNC animated:YES completion:nil];
}


#pragma mark MKMapView Delegate


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MyAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];

        MyAnnotation *showAnnotation = [[MyAnnotation alloc]init];
        
        for (MyAnnotation *myAnnotation in self.locationsArray) {
            if ([myAnnotation isEqual:annotation]) {
                showAnnotation = annotation;
            }
        }
        
        // If an existing pin view was not available, create one.
        pinView = [[MKAnnotationView alloc] initWithAnnotation:showAnnotation reuseIdentifier:@"PinAnnotationView"];
        
        pinView.canShowCallout = YES;
        
        if (showAnnotation.pinImageIndex) {
            int index = [showAnnotation.pinImageIndex intValue];
            
            pinView.image = [self.pinImagesArray objectAtIndex:index];
        }else{
            pinView.image = [UIImage imageNamed:@"pinIcon"];
        }
        
        pinView.calloutOffset = CGPointMake(-5, 0);
        
        // Add a detail disclosure button to the callout.
        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        pinView.rightCalloutAccessoryView = rightButton;
        
        // Add an image to the left callout.
        UIImage *photo;
        if (self.currentPhoto == nil) {
            photo = showAnnotation.photo;
        }else{
            photo = self.currentPhoto;
            self.currentPhoto = nil;
        }
        
        
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5,70, 50)];
        iconView.image = photo;
        iconView.clipsToBounds = YES;
        iconView.layer.cornerRadius = 5.0;
        iconView.contentMode = UIViewContentModeScaleAspectFill;
        
        pinView.leftCalloutAccessoryView = iconView;
        
        showAnnotation = nil;
        
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    if (CLLocationCoordinate2DIsValid(region.center)){
        self.currentRegion = region;
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    }
}
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    if ([control isKindOfClass:[UIButton class]]) {
       // AnnotationViewControll
    }
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    self.selectedAnnotation = (MyAnnotation *)view.annotation;
}

-(void)rightButtonPressed{
    UINavigationController *annotationNC = [self.storyboard instantiateViewControllerWithIdentifier:@"annotationNC"];
    AnnotationViewController *annotationVC = (AnnotationViewController *)annotationNC.topViewController;
    annotationVC.annotation = self.selectedAnnotation;
    annotationVC.canEdit = NO;
    annotationVC.delegate = self;
    [self presentViewController:annotationNC animated:YES completion:nil];
}

#pragma mark AnnotationView Delegate
- (void)didSaveNewAnnotationWithImage:(UIImage *)image TitleAnnotation:(NSString *)title Subtitle:(NSString *)subtitle Filter:(NSInteger)filter Owner:(NSString *)owner PinImageIndex:(NSNumber *)pinImageIndex{
    
    PFUser *currentUser = [PFUser currentUser];
    
    self.saveAnnotation = [[MyAnnotation alloc]init];
    self.saveAnnotation.coordinate = self.currentRegion.center;
    self.saveAnnotation.title =  title;
    self.saveAnnotation.subtitle = [NSString stringWithFormat:@"(%@) %@", currentUser.username, subtitle];
    self.saveAnnotation.photo = image;
    self.saveAnnotation.pinImageIndex = pinImageIndex;
    self.saveAnnotation.owner = owner;
    self.saveAnnotation.filterIndex = filter;
    self.currentPhoto = image;
    
    NSString *stringFilter;
    switch (filter) {
        case 0:
            stringFilter = @"You";
            break;
        case 1:
            stringFilter = @"Couple";
            break;
        case 2:
            stringFilter = @"Friends";
            break;
        case 3:
            stringFilter = @"Public";
            break;
            
        default:
            break;
    }
    
    self.saveAnnotation.filter = stringFilter;
    
    [self.manager numberOfLocationsForUser];
}

#pragma mark RequestManager Delegate
-(void)didReceivedAllLocations:(NSMutableArray *)locations{

    [self.mapView removeAnnotations:self.locationsArray];
    [self.locationsArray removeAllObjects];
    
    [self.locationsArray addObjectsFromArray:locations];
    [self.mapView addAnnotations:self.locationsArray];
}

-(void)didSavedLocation:(MyAnnotation *)annotation{
    if ([annotation.objectId length]) {
        [self.locationsArray addObject:annotation];
        [self.mapView addAnnotation:annotation];
    }else{
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Could not saved this location"
                                                              message:@"Try again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

-(void)didUpdateUser:(BOOL)succeeded{
    if (succeeded) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"User updated"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];

    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"User could not be updated"
                                                              message:@"Try again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];

    }
}

-(void)didGetCouple:(NSString *)couple{
    
    if (couple.length) {
        [self.manager checkUserForAddCouple:couple WithFlag:NO];
    }else{
        [self.manager getFriendsForUser];
    }
    
}

-(void)didGetFriendsForUser:(NSMutableArray *)userIds{
   
    if (userIds.count) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        NSString *userName = [[userDefault objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length > 0) {
            [[userDefault objectForKey:@"UserApp"]setObject:userIds forKey:@"friends"];
        }else{
            [[userDefault objectForKey:@"UserFacebook"]setObject:userIds forKey:@"friends"];
        }
        [userDefault synchronize];
    }
}

-(void)didSetDefaultImage:(BOOL)succeeded{
    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:[UIImage imageNamed:@"LocationTestLogo"] forKey:@"imageProfile"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [self.manager getCoupleForUser];
    self.signedUpObjectId = nil;
}

-(void)didReceivedUserForCouple:(PFObject *)user{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    PFUser *couple = (PFUser *)user;
    NSMutableDictionary *coupleDict = [NSMutableDictionary new];
    
    PFFile *file = [couple objectForKey:@"image"];
    NSString *imageUrl = file.url;
    
    [coupleDict setObject:couple.username forKey:@"username"];
    [coupleDict setObject:couple.email forKey:@"email"];
    [coupleDict setObject:couple.objectId forKey:@"objectId"];
    [coupleDict setObject:imageUrl forKey:@"imageProfile"];
    
    NSString *userName = [[userDefault objectForKey:@"UserApp"]valueForKey:@"username"];
    if (userName.length > 0) {
        [[userDefault objectForKey:@"UserApp"]setObject:coupleDict forKey:@"couple"];
    }else{
        [[userDefault objectForKey:@"UserFacebook"]setObject:coupleDict forKey:@"couple"];
    }
    [userDefault synchronize];
    
    [self.manager getFriendsForUser];
}

-(void)hasNoConnection{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Device has no internet connection"
                                          message:@"Please connect device to internet"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)didGetNumberOfLocations:(NSUInteger)number{
    
    if (number < 2) {
        [self.manager saveLocation:self.saveAnnotation];
    }else{
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"If you want to save an other locations, you must purchase PRO version"
                                              message:@"And you will have more improvement"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"PURCHASE"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
#warning CHANGE URL WITH THE PRO VERSION APP URL IN ITUNNES
                                       [[UIApplication sharedApplication]
                                        openURL:[NSURL URLWithString:@"http://www.google.es"]];
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];

        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];

    }
}

-(void)didReceivedYourLocations:(NSMutableArray *)locations{
    
    [settingsPopoverController dismissPopoverAnimated:YES];
    settingsPopoverController.delegate = nil;
    settingsPopoverController = nil;
    
    UINavigationController  *locationListNC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationsListNC"];
    LocationsListViewController *locationsListVC = (LocationsListViewController *)locationListNC.topViewController;
    locationsListVC.delegate = self;
    locationsListVC.locationsArray = locations;
    
    [self presentViewController:locationListNC animated:YES completion:nil];
}

#pragma mark PFLogInViewController Delegate

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user{
    
    PFFile *imageFile = user[@"image"];
    NSString *imageUrl = [NSString stringWithFormat:@"%@", imageFile.url];
    
    NSDictionary *couple = [[NSDictionary alloc]init];
    if ([user objectForKey:@"couple"]) {
        couple = [[user objectForKey:@"couple"]objectAtIndex:0];
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userDict = @{
                               @"objectId" : user.objectId,
                               @"imageProfile" : imageUrl,
                               @"username" : user.username,
                               @"email" : user.email,
                               @"couple" : couple};
    
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults removeObjectForKey:@"UserFacebook"];
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)signUpViewController:(PFSignUpViewController * __nonnull)signUpController didSignUpUser:(PFUser * __nonnull)user{
    
    self.isSignedUp = YES;
    self.signedUpObjectId = user.objectId;
    
    NSDictionary *couple = [[NSDictionary alloc]init];
    PFFile *imageFile = user[@"image"];
    NSString *imageUrl = [NSString stringWithFormat:@"%@", imageFile.url];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userDict = @{
                               @"objectId" : user.objectId,
                               @"imageProfile" : imageUrl,
                               @"username" : user.username,
                               @"email" : user.email,
                               @"couple" : couple};
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults removeObjectForKey:@"UserFacebook"];
    [defaults synchronize];
    
    [self.locationsArray removeAllObjects];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Facebook API

- (void)_loginWithFacebook {
    
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email"];
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (user.isNew) {
            NSString *objectId = user.objectId;
            [self _loadDataWithId:objectId];
            NSLog(@"User signed up and logged in through Facebook!");
        } else {
            NSString *objectId = user.objectId;
            [self _loadDataWithId:objectId];
            NSLog(@"User logged in through Facebook!");
        }
    }];
}


- (void)_loadDataWithId:(NSString *)objectId {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            NSDictionary *couple = [[NSDictionary alloc]init];

            NSMutableDictionary *userData = [(NSDictionary *)result mutableCopy];
            [userData setObject:objectId forKey:@"objectId"];
            [userData setObject:couple forKey:@"couple"];
            
            NSString *facebookID = userData[@"id"];
            NSString *name = userData[@"name"];
            NSString *location = userData[@"location"][@"name"];
            NSString *gender = userData[@"gender"];
            NSString *birthday = userData[@"birthday"];
            NSString *email = userData[@"email"];
            [userData removeObjectForKey:@"relationship_status"];
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            NSString *imageUrl = [NSString stringWithFormat:@"%@", pictureURL];
            [userData setObject:imageUrl forKey:@"imageProfile"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            

            [defaults setObject:userData forKey:@"UserFacebook"];
            [defaults removeObjectForKey:@"UserApp"];
            [defaults synchronize];
            

            [self changeUserNameToFacebookName:name withEmail:email withId:objectId];
        }
        
    }];
    
    
}

-(void)changeUserNameToFacebookName:(NSString *)facebookName withEmail:(NSString *)email withId:(NSString *)objectId{
    
    PFQuery *query = [PFUser query];
    
    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]setObject:facebookName forKey:@"username"];
    [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]setObject:email forKey:@"email"];
    
    [query whereKey:@"objectId" equalTo:objectId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
        // Now let's update it with some new data. In this case, only cheatMode and score
        // will get sent to the cloud. playerName hasn't changed.
        
        user[@"username"] = facebookName;
        user[@"email"] = email;
        
        [user saveInBackground];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        self.signedUpObjectId = objectId;
        self.isSignedUp = YES;
    }];
    
}

#pragma mark Set Interstitial AD

-(void) setInterstitialAd{
    
    self.adIntersticialView = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8715619194809926/2849378497"];
    self.adIntersticialView.delegate = self;
    
    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADInterstitial automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [self.adIntersticialView loadRequest:request];
}

#pragma mark GADIntersticialDelegate

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    
    
    [self.adIntersticialView presentFromRootViewController:self];
    
}


@end
